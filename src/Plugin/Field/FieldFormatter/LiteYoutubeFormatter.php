<?php

namespace Drupal\lite_youtube\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\media\Entity\MediaType;
use Drupal\media\Plugin\media\Source\OEmbedInterface;
use Drupal\youtube\Plugin\Field\FieldType\YouTubeItem;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'lite_youtube' formatter.
 *
 * Se utiliza para procesar los videos de tipo youtube y de multimedia.
 *
 * @FieldFormatter(
 *   id = "lite_youtube",
 *   label = @Translation("Lite YouTube"),
 *   field_types = {
 *     "youtube",
 *     "link",
 *     "string",
 *     "string_long",
 *   },
 * )
 */
class LiteYoutubeFormatter extends FormatterBase {

  /**
   * The logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, LoggerChannelFactoryInterface $logger_factory) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->logger = $logger_factory->get('media');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [
      'youtube_video_play' => 'Play',
      'youtube_poster_quality' => 'hqdefault',
      'youtube_poster_loading' => 'lazy',
      'youtube_nocookie' => 'false',
      'youtube_autoload' => 'false',
      'youtube_short' => 'false',
      'youtube_params' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state): array {
    $elements = parent::settingsForm($form, $form_state);

    $elements['youtube_video_play'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Video play'),
      '#description' => $this->t('The title of the play button (for translation).'),
      '#size' => 10,
      '#default_value' => $this->getSetting('youtube_video_play'),
    ];

    $elements['youtube_poster_quality'] = [
      '#type' => 'select',
      '#title' => $this->t('Poster quality'),
      '#description' => $this->t('Set thumbnail poster quality (maxresdefault, sddefault, mqdefault, hqdefault).'),
      '#options' => $this->getPosterQualities(),
      '#default_value' => $this->getSetting('youtube_poster_quality'),
    ];

    $elements['youtube_poster_loading'] = [
      '#type' => 'select',
      '#title' => $this->t('Poster loading'),
      '#description' => $this->t('Set img lazy load attr loading for poster image.'),
      '#options' => [
        'eager' => $this->t('eager'),
        'lazy' => $this->t('lazy'),
      ],
      '#default_value' => $this->getSetting('youtube_poster_loading'),
    ];

    $elements['youtube_nocookie'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('Use youtube-nocookie.com as iframe embed uri.'),
      '#title' => $this->t('Nocookie'),
      '#default_value' => $this->getSetting('youtube_nocookie'),
    ];

    $elements['youtube_autoload'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('Use Intersection Observer to load iframe when scrolled into view.'),
      '#title' => $this->t('Autoload'),
      '#default_value' => $this->getSetting('youtube_autoload'),
    ];

    $elements['youtube_short'] = [
      '#type' => 'checkbox',
      '#description' => $this->t('Show 9:16 YouTube Shorts-style interaction on mobile devices.'),
      '#title' => $this->t('Short'),
      '#default_value' => $this->getSetting('youtube_short'),
    ];

    $elements['youtube_params'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Params'),
      '#description' => $this->t('Set YouTube query parameters'),
      '#default_value' => $this->getSetting('youtube_video_play'),
    ];
    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary(): array {
    $videoPlay = $this->getSetting('youtube_video_play');
    $posterQuality = $this->getSetting('youtube_poster_quality');
    $posterQualities = $this->getPosterQualities();
    $posterQualityLabel = $posterQualities[$posterQuality] ?? '';
    $posterLoading = $this->getSetting('youtube_poster_loading');
    return [
      $this->t('Video Play: @videoPlay', ['@videoPlay' => $videoPlay]),
      $this->t('Poster Quality: @posterQuality', ['@posterQuality' => $posterQualityLabel]),
      $this->t('Poster Loading: @posterLoading', ['@posterLoading' => $posterLoading]),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $element = [];
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      $video_id = NULL;
      try {
        if ($item instanceof YouTubeItem) {
          $video_id = $item->video_id;
        }
        else {
          $main_property = $item->getFieldDefinition()
            ->getFieldStorageDefinition()
            ->getMainPropertyName();
          $value = $item->{$main_property};
          $video_id = youtube_get_video_id($value);
        }
      }
      catch (\Exception $e) {
        $this->logger->error("Failed to process video item with message: @message.", ['@message' => $e->getMessage()]);
        continue;
      }

      $element[$delta] = [
        '#theme' => 'lite_youtube',
        '#video_id' => $video_id,
        '#entity_title' => $items->getEntity()->label(),
        '#settings' => $settings,
        '#attached' => ['library' => ['lite_youtube/lite_youtube']],
      ];
    }
    return $element;
  }

  /**
   * Get poster qualities.
   */
  protected function getPosterQualities(): array {
    return [
      'maxresdefault' => 'Max',
      'sddefault' => 'SD',
      'mqdefault' => 'MQ',
      'hqdefault' => 'HQ',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition): bool {
    if ($field_definition->getTargetEntityTypeId() !== 'media') {
      return $field_definition->getType() == 'youtube';
    }

    if (parent::isApplicable($field_definition)) {
      $media_type = $field_definition->getTargetBundle();

      if ($media_type) {
        $media_type = MediaType::load($media_type);
        return $media_type && $media_type->getSource() instanceof OEmbedInterface;
      }
    }
    return FALSE;
  }

}
